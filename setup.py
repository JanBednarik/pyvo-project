from os import path
from setuptools import setup


def read_requirements(filename):
    specifiers = []
    dep_links = []
    with open(filename, 'r') as f:
        for line in f:
            if line.startswith(('#', '-r')) or line.strip() == '':
                continue
            if line.startswith('git+'):
                dep_links.append(line.strip())
            else:
                specifiers.append(line.strip())
    return specifiers, dep_links


setup_py_path = path.dirname(path.realpath(__file__))
requirements, dep_links = read_requirements(path.join(setup_py_path, 'requirements.txt'))
test_requirements, _ = read_requirements(path.join(setup_py_path, 'test-requirements.txt'))

setup(
    name='Pyvo Test',
    version='0.1',
    description='',
    long_description='',
    author='Martin Curlej',
    author_email='martin.curlej@gmail.com',
    install_requires=requirements,
    dependency_links=dep_links,
    test_require=test_requirements,
)
